# Small Python Scripts

To document various python basic scripts

# cropImages.py (2020)
Program to create x amount of crops from y amount of images:

- loads all .jpg's from current directory and puts them into array
- uses while loop to decide how many crops we need
- random function used to decide where to crop, not how big the crop should be, cf. 'cutSize'
- crops saved in folder: 'imgCrops'

## turnPNGtoGIF.py (2020)

Convert all the PNG files into one animated GIF.
- a txt file with all the png images (called image_list.txt)
- a for-loop to cycle all the individual image
- image conversion

## rename.py (2018)
The script involves two simple programs:
1. Rename files and remove files according to a particular pattern of files
2. Rename the files with 4 digit incremental count, and it starts with 0026

## split.py (2018)
python_split: Simply showing how a split function works within a for loop in Python

```python
str_split = """fears:0.991, anger:0.000991, happiness:0.333336, sadness:0.000004
fears:0.091, anger:0.000991, happiness:0.033336, sadness:0.010004
fears:0.001, anger:0.000991, happiness:0.003336, sadness:0.0004"""
str_temp = ''
for line in str_split.splitlines():
	str_set = line.split(',')
	for i in str_set:
		str_now = i.split(':')[1]
		str_temp = str_temp + str_now + ','
	print(str_temp)
	str_temp=''
```

OUTPUT
```
0.991,0.000991,0.333336,0.000004,
0.091,0.000991,0.033336,0.010004,
0.001,0.000991,0.003336,0.0004,
```

### EXPLANATION
1. With a given text that is stored in a variable 'str_split', you can see some kind of pattern that can be splitted by ',' and ':'
2. First is to read each line by using splitlines()
3. Then within each line, it will further split by ',' in order to get the individual set of parameters (as array)
3. loop through the arrays and then split by ':' to get the exact numeric values by specifying the array index[1] (see line 12)
4. Align the whole numeric extraction within a same line as output

## SPEED SHOW (2013)

Two small python scripts that detect idle time from mac os (mouse and keyboard), and then it forces the computer visits a specific website with Firefox web browser.

### Two scripts
idleTime.py:
> call terminal and get the idle time from mac os

checkinactive.py:
> This is an integrated python file: check idle time + check firefox active/inactive status + kill browser + load page with a firefox browser

### Tested configuration
It works in Mac OS X 10.5.8 + python 2.7

### Operations
To quit the application, just press Control+C. Else, it will continue to count the idle time

- Instead of using os.system to run the command line, I have to use os.popen to fetch the output screen(it is a perl command)

- Since I just need to get the time in 'second', I just split the return value by '.' and then get the first bit of the array to roughly indicate as the idle time in second
