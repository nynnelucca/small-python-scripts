import glob
import os

gif_name = 'mao2020'
# Get all the pngs in the current directory
file_list = glob.glob('*.png')
#list.sort(file_list, key=lambda x: int(x.split('_')[1].split('.png')[0]))
# Sort the images by #, this may need to be tweaked for your use case
list.sort(file_list)
with open('image_list.txt', 'w') as file:
    for item in file_list:
        file.write("%s\n" % item)
# just like the terminal to convert the image file using 'imagemagick'
os.system('convert @image_list.txt {}.gif'.format(gif_name))
